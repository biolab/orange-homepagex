#!/usr/bin/env python

import os, subprocess, sys, tempfile, urllib

root = os.path.join(os.path.dirname(__file__), '..')
requirements = os.path.abspath(os.path.join(root, 'requirements.txt'))
manage_script = os.path.abspath(os.path.join(root, 'manage.py'))

print "Installing requirements:\n"
subprocess.check_call(('pip', 'install', '-r', requirements))

#print "\nSetting up the database:\n"
#    if os.path.isfile(database_file)
#os.remove(database_file)
#subprocess.check_call(('python', manage_script, 'syncdb'))
#subprocess.check_call(('python', manage_script, 'migrate'))
#subprocess.check_call(('python', manage_script, 'reset', '--noinput', 'contenttypes'))

print "\nDownloading and importing database dump:\n"

subprocess.check_call('psql', '-U postgres -c "CREATE USER orange_webpage;"')
subprocess.check_call('psql', '-U postgres -c "DROP DATABASE orange_webpage;"')
subprocess.check_call('psql', '-U postgres -c "CREATE DATABASE orange_webpage OWNER orange_webpage;"')

tempFile = open('db.sql', 'w')
webFile = urllib.urlopen('http://new.orange.biolab.si/db/')
tempFile.write(webFile.read())
webFile.close()
tempFile.close()

subprocess.check_call('/usr/local/bin/pg_restore', '-U cms -h /tmp -d orange_webpage db.sql')

print "\nAll done!"
