import os
import glob
import xml.dom.minidom

from django.db import models
from cms import models as cms_models
from django.conf import settings

doc = xml.dom.minidom.parse(file(settings.SCREENSHOTS_XML_FILE))

class Screenshot(object):
    def __init__(self, **argv):
        self.__dict__.update(argv)

screenshots = []
features_screenshots = []
for node in doc.getElementsByTagName('screenshot'):
    id = node.getAttribute('id')
    title = node.getAttribute('title')
    hide = node.getAttribute('hide')
    img = node.getAttribute('img') or ('snp-%s.png' % id)
    rank = int(node.getAttribute('rank') or 999)
    thumb = node.getAttribute('thumb') or ('tbn-%s.png' % id)
    features = node.getAttribute('features')
    if not hide == 'yes':
        screenshots.append((rank, Screenshot(id=id, img=img, rank=rank, thumb=thumb, title=title)))
    if features:
        features_screenshots.append((rank, Screenshot(id=id, img=img, rank=rank, thumb=thumb, title=title, features=features)))
screenshots = [s for _, s in sorted(screenshots)]
features_screenshots = [s for _, s in sorted(features_screenshots)]
preview_images = [{'src': i[len(settings.PREVIEW_ROOT) + 1:], 'id': i[len(settings.PREVIEW_ROOT) + 1:-4]} for i in glob.glob(settings.PREVIEW_PATTERN)]

chapters = (
    ('vis_prog', "Visual programming"),
    ('visualization', "Visualization"),
    ('interaction', "Interaction and data analytics"),
    ('tools', "Large toolbox"),
    ('scripting', "Scripting interface"),
    ('platforms', "Platform independence"),
)

class FeaturePlugin(cms_models.CMSPlugin):
    feature = models.CharField(max_length=30, choices=chapters)

    def __unicode__(self):
        return self.get_feature_display()

    def get_screenshots(self):
        for scr in features_screenshots:
            if scr.features == self.feature:
                yield scr

def download_choices():
    for fn in ['win', 'mac']:
        ffi = open(settings.DOWNLOAD_SET_PATTERN % fn, 'rt')
        for line in ffi:
            ep = line.find('=')
            key = line[:ep].strip()
            value = line[ep+1:].strip()
            if value:
                if fn == 'win':
                    for py in ['2.5', '2.6', '2.7']:
                        pykey = '%s_PY%s' % (key, py)
                        yield (pykey, pykey)
                yield (key, key)
        ffi.close()

class DownloadPlugin(cms_models.CMSPlugin):
    download = models.CharField(max_length=100, choices=download_choices())
    label = models.CharField(max_length=255)

    def __unicode__(self):
        return self.label
