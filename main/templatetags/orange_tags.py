import datetime
import os
import os.path

from django import template

DOWNLOADS_DIR = '/srv/downloads'

register = template.Library()

@register.inclusion_tag('main/download.html', takes_context=True)
def download_button(context):
    try:
        request = context['request']
        agent = request.META['HTTP_USER_AGENT']
        for os_name in ["Windows", "Mac OS X", "Linux"]:
            if os_name in agent:
                break
        else:
            os_name = "Windows" # Default
    
        for l in file(os.path.join(DOWNLOADS_DIR, "os_names.txt")):
            try:
                os_n, os_name_web, infile, envname = l.strip().split("\t")
                if os_n.strip() == os_name:
                    break
            except:
                pass
        else:
            raise ValueError # Let the handler bellow handle it
    
        for l in file(os.path.join(DOWNLOADS_DIR, infile)):
            try:
                varname, dlfile = l.strip().split("=")
                if varname == envname:
                    if os_name == "Windows" and not os.path.splitext(dlfile)[1]:
                        dlfile += "-py2.7.exe"
                    builddate = datetime.datetime.fromtimestamp(os.path.getmtime(os.path.join(DOWNLOADS_DIR, dlfile)))
                    break
            except:
                pass
        else:
            raise ValueError # Let the handler bellow handle it

    except:
        os_name_web = "Download Orange"
        dlfile = ""
        builddate = None

    context.update({
        'os_name_web': os_name_web,
        'dlfile': dlfile,
        'builddate': builddate,
    })

    return context

