import glob, os

from django.conf import settings
from django.utils.translation import ugettext_lazy as _

from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from main import models

showcases_images = [i[len(settings.SCREENSHOTS_ROOT) + 1:] for i in glob.glob(settings.SCREENSHOTS_PATTERN)]

class LicensePlugin(CMSPluginBase):
    """
    Displays a license file.
    """

    name = _('License file')
    render_template = 'main/license.html'

    def render(self, context, instance, placeholder):
        in_other = False
        text = ""
        other = []
        for l in file(settings.LICENSE_FILE):
            if l.startswith('----'):
                in_other = not in_other
                if in_other:
                    other.append(l)
                else:
                    other[-1] += l.rstrip()
            elif in_other:
                other[-1] += l
            else:
                text += l

        context.update({
            'text': text,
            'other': other,
        })

        return context

plugin_pool.register_plugin(LicensePlugin)

class FeaturePlugin(CMSPluginBase):
    """
    Displays feature screenshots.
    """

    model = models.FeaturePlugin
    name = _('Feature screenshots')
    render_template = "main/feature_screenshots.html"

    def render(self, context, instance, placeholder):
        context.update({
            'screenshots': instance.get_screenshots(),
        })

        return context

plugin_pool.register_plugin(FeaturePlugin)

class ScreenshotsPlugin(CMSPluginBase):
    """
    Displays screenshots.
    """

    name = _('Screenshots')
    render_template = 'main/screenshots.html'

    def render(self, context, instance, placeholder):
        context.update({
            'screenshots': models.screenshots,
        })

        return context

plugin_pool.register_plugin(ScreenshotsPlugin)

class PreviewPlugin(CMSPluginBase):
    """
    Displays preview.
    """

    name = _('Preview')
    render_template = 'main/preview.html'

    def render(self, context, instance, placeholder):
        context.update({
            'preview_images': models.preview_images,
        })

        return context

plugin_pool.register_plugin(PreviewPlugin)

class ShowcasesPlugin(CMSPluginBase):
    """
    Displays showcases.
    """

    name = _('Showcases')
    render_template = 'main/showcases.html'

    def render(self, context, instance, placeholder):
        context.update({
            'showcases': showcases_images,
        })

        return context

plugin_pool.register_plugin(ShowcasesPlugin)

class DownloadPlugin(CMSPluginBase):
    """
    Displays a link to Orange download file.
    """

    model = models.DownloadPlugin
    name = _('Download link')
    render_template = 'main/download_link.html'
    text_enabled = True

    def _get_link(self, selected_key):
        py = None
        if len(selected_key) >= 6 and selected_key[-6:] in ['_PY2.5', '_PY2.6', '_PY2.7']:
            py = selected_key[-3:]
            selected_key = selected_key[:-6]

        for fn in ['win', 'mac']:
            ffi = open(settings.DOWNLOAD_SET_PATTERN % fn, 'rt')
            for line in ffi:
                ep = line.find('=')
                key = line[:ep].strip()
                value = line[ep+1:].strip()
                if key == selected_key:
                    if py:
                        return '%s-py%s.exe' % (value, py)
                    else:
                        return value
            ffi.close()

        return None

    def render(self, context, instance, placeholder):
        context.update({
            'link': self._get_link(instance.download),
            'label': instance.label,
        })
        return context

    def icon_src(self, instance):
        return settings.CMS_MEDIA_URL + u'images/plugins/link.png'

plugin_pool.register_plugin(DownloadPlugin)
