django==1.5.1
psycopg2==2.5
wsgiref==0.1.2
whoosh==2.4.1
-e hg+https://bitbucket.org/shagi/django-sphinxdoc/@4fef395#egg=django-sphinxdoc
-e git+https://github.com/toastdriven/django-haystack.git@cd233d6c3d6e5fb786d24cc968ffa0e8d1bd4e23#egg=django-haystack
django-sekizai==0.7
pil==1.1.7
django-classy-tags==0.4
django-reversion==1.7
easy-thumbnails==1.2
django_polymorphic==0.4.2
django-filer==0.9.4
django-cms==2.4.2
cmsplugin-filer==0.9.5
django-missing==0.1.11
cmsplugin-markup==0.2.4
Trac==1.0.1
cmsplugin-markup-tracwiki==0.2.4
#cmsplugin-blog==1.1.2
#-e git+https://github.com/fivethreeo/cmsplugin-blog.git@028a803acd7722af0390e37505bbb7403e99867f#egg=cmsplugin-blog
#-e git+https://github.com/Tyrdall/cmsplugin-blog.git@dd83811aee408807969999bd5b2598cfc4d1c157#egg=cmsplugin-blog
#-e git+https://github.com/daneoshiga/cmsplugin-blog.git@028a803acd7722af0390e37505bbb7403e99867f#egg=cmsplugin-blog
-e git+https://github.com/gonzalodelgado/cmsplugin-blog.git@655a109c0b7e4b2bf395231179278337bdd4ba48#egg=cmsplugin-blog
cmsplugin-contact==1.0.0
recaptcha-client==1.0.6
pygments==1.6
django-tinymce==1.5.1
-e svn+http://trac-hacks.org/svn/dashessyntaxplugin/0.11/#egg=dashessyntaxplugin
-e svn+http://trac-hacks.org/svn/footnotemacro/trunk/#egg=footnotemacro
-e git+https://github.com/mitar/trac-mathjax.git@e5b2bcbd8ec74685407c6fb2e71fb56cc2f47484#egg=mathjax
-e hg+https://bitbucket.org/kisielk/tracmathplugin/@2596545#egg=tracmathplugin
