# Django settings for orange_website project.
import os
import socket

# Secrets are in a separate file so they are not visible in public repository
from secrets import *

PROJECT_PATH = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
DOWNLOAD_SET_PATTERN = os.path.join(PROJECT_PATH, 'download/filenames_%s.set')
LICENSE_FILE = os.path.join(PROJECT_PATH, 'LICENSES')
SCREENSHOTS_ROOT = os.path.join(PROJECT_PATH, 'main', 'static', 'screenshots')
SCREENSHOTS_XML_FILE = os.path.join(SCREENSHOTS_ROOT, 'screenshots.xml')
SCREENSHOTS_PATTERN = os.path.join(SCREENSHOTS_ROOT, 'shc-*.png')
PREVIEW_ROOT = os.path.join(PROJECT_PATH, 'main', 'static', 'preview')
PREVIEW_PATTERN = os.path.join(PREVIEW_ROOT, '*.png')

DEBUG = True
TEMPLATE_DEBUG = DEBUG

if socket.gethostname() == 'biolab':
#    DEBUG = False
#    TEMPLATE_DEBUG = False
    DOWNLOAD_SET_PATTERN = '/srv/downloads/filenames_%s.set'

ADMINS = (
     ('Miha Stajdohar', 'miha.stajdohar@fri.uni-lj.si'),
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',     # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'orange_website',                               # Or path to database file if using sqlite3.
        'USER': 'orange_website',                               # Not used with sqlite3.
        'PASSWORD': '',
        'HOST': '',                      # Empty for localhost through domain sockets or '127.0.0.1' for localhost through TCP.
        'PORT': '',                      # Set to empty string for default.
    }
}

# Hosts/domain names that are valid for this site; required if DEBUG is False
# See https://docs.djangoproject.com/en/1.5/ref/settings/#allowed-hosts
ALLOWED_HOSTS = ['193.2.72.56']

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'Europe/Ljubljana'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en'

LANGUAGES = (
    ('en', 'English'),
)

# Date input formats below take as first argument day and then month in x/y/z format
DATE_INPUT_FORMATS = (
    '%Y-%m-%d', '%d/%m/%Y', '%d/%m/%y', '%b %d %Y',
    '%b %d, %Y', '%d %b %Y', '%d %b, %Y', '%B %d %Y',
    '%B %d, %Y', '%d %B %Y', '%d %B, %Y',
)

# All those formats are only defaults and are localized for users
DATE_FORMAT = 'd/M/Y'
TIME_FORMAT = 'H:i'
DATETIME_FORMAT = 'd/M/Y, H:i'
YEAR_MONTH_FORMAT = 'F Y'
MONTH_DAY_FORMAT = 'j F'
SHORT_DATE_FORMAT = 'd/m/y'
SHORT_DATETIME_FORMAT = 'd/m/y H:i'
FIRST_DAY_OF_WEEK = 1
DECIMAL_SEPARATOR = '.'
THOUSAND_SEPARATOR = ','
NUMBER_GROUPING = 0

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/var/www/example.com/media/"
MEDIA_ROOT = os.path.join(PROJECT_PATH, 'media')

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://example.com/media/", "http://media.example.com/"
MEDIA_URL = '/media/'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/var/www/example.com/static/"
STATIC_ROOT = os.path.join(PROJECT_PATH, 'static')
# URL prefix for static files.
# Example: "http://example.com/static/", "http://static.example.com/"
STATIC_URL = '/static/'

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    '{0}/{1}/{2}'.format(PROJECT_PATH, 'orange_website', 'static'),
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.request',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.core.context_processors.tz',
    'django.contrib.messages.context_processors.messages',
    'cms.context_processors.media',
    'sekizai.context_processors.sekizai',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.contrib.redirects.middleware.RedirectFallbackMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.doc.XViewMiddleware',
    'django.middleware.common.CommonMiddleware',
    'cms.middleware.page.CurrentPageMiddleware',
    'cms.middleware.user.CurrentUserMiddleware',
    'cms.middleware.toolbar.ToolbarMiddleware',
    'cms.middleware.language.LanguageCookieMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'orange_website.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'orange_website.wsgi.application'

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    '{0}/{1}/{2}'.format(PROJECT_PATH, 'orange_website', 'templates'),
)

INSTALLED_APPS = (
    #'cmsplugin_markup_tracwiki',

    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    'django.contrib.admindocs',
    'django.contrib.markup',
    'django.contrib.redirects',

    'cms',
    'mptt',
    'menus',
    'south',
    'easy_thumbnails',
    'filer',
    'sekizai',
    'cms.plugins.flash',
    'cms.plugins.googlemap',
    'cms.plugins.link',
    'cms.plugins.text',
    'cms.plugins.twitter',
    'cmsplugin_filer_file',
    'cmsplugin_filer_folder',
    'cmsplugin_filer_image',
    'cmsplugin_filer_teaser',
    'cmsplugin_filer_video',
    'cmsplugin_markup',
    'cmsplugin_blog',
    'cmsplugin_contact',

    'djangocms_utils',
    'simple_translation',
    'tagging',
    'reversion',

    'missing',
    'haystack',
    'sphinxdoc',
    'main',
)

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

SERVER_EMAIL = 'Orange Support <webmaster@biolab.si>'
EMAIL_SUBJECT_PREFIX  = '[Orange] '

THUMBNAIL_PROCESSORS = (
    'easy_thumbnails.processors.colorspace',
    'easy_thumbnails.processors.autocrop',
    #'easy_thumbnails.processors.scale_and_crop',
    'filer.thumbnail_processors.scale_and_crop_with_subject_location',
    'easy_thumbnails.processors.filters',
)

CMS_TEMPLATES = (
    ('main/simple.html', 'Simple Page'),
    ('main/double.html', 'Double Page'),
    ('main/blog.html', 'Blog Page'),
    ('main/main.html', 'Main Page'),
)

HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'haystack.backends.whoosh_backend.WhooshEngine',
        'PATH': os.path.join(PROJECT_PATH, 'whoosh_index'),
    },
}

CMSPLUGIN_BLOG_PLACEHOLDERS = ('on_index_page', 'the_rest')

CMS_PLACEHOLDER_CONF = {
    'content': {
        'plugins': ['MarkupPlugin', 'TextPlugin', 'PicturePlugin'],
        'text_only_plugins': ['LinkPlugin'],
        'name':"Content",
    },
}

JQUERY_JS = '%smain/jquery/jquery.min.js' % STATIC_URL
JQUERY_UI_CSS = '%smain/jquery/jquery-ui.min.css' % STATIC_URL
JQUERY_UI_JS = '%smain/jquery/jquery-ui.min.js' % STATIC_URL

CMS_LANGUAGES = LANGUAGES
CMS_PERMISSION = False
CMS_MODERATOR = False
CMS_MENU_TITLE_OVERWRITE = False
CMS_SOFTROOT = False
CMS_SHOW_START_DATE = CMS_SHOW_END_DATE = False
CMS_SEO_FIELDS = True # my
CMS_URL_OVERWRITE = True
CMS_REDIRECTS = False
CMS_APPHOOKS = ()
CMS_MEDIA_PATH = 'cms/'
CMS_MEDIA_ROOT = MEDIA_ROOT + CMS_MEDIA_PATH
CMS_MEDIA_URL= MEDIA_URL + CMS_MEDIA_PATH
CMS_PAGE_MEDIA_PATH = 'cms_page_media/'

CMS_USE_TINYMCE = True


CMS_MARKUP_OPTIONS = (
    'cmsplugin_markup_tracwiki',
    'cmsplugin_markup.plugins.markdown',
    'cmsplugin_markup.plugins.textile',
    'cmsplugin_markup.plugins.restructuredtext',
)

RESTRUCTUREDTEXT_FILTER_SETTINGS = {
    'initial_header_level': 2,
}

CMS_MARKUP_TRAC_INTERTRAC = {
    'trac': {
        'TITLE': 'Orange Trac',
        'URL': 'http://orange.biolab.si/trac',
    },
}

CMS_MARKUP_TRAC_INTERWIKI = {
    'skypechat': {
        'URL': 'skype:?chat&blob=',
    },
    'wikipedia': {
        'URL': 'http://en.wikipedia.org/wiki/',
    },
}

CMS_MARKUP_TRAC_CONFIGURATION = {
    'tracmath': {
        'cache_dir': os.path.join(PROJECT_PATH, 'tracwiki', 'cache'),
    }
}

CMS_MARKUP_TRAC_TEMPLATES_DIR = os.path.join(PROJECT_PATH, 'tracwiki', 'templates')

CMS_MARKUP_TRAC_COMPONENTS = (
    'tracdashessyntax.plugin.DashesSyntaxPlugin',
    'footnotemacro.macro.FootNoteMacro',
    'mathjax.api.MathJaxPlugin',
    'tracmath.tracmath.TracMathPlugin',
)

CMS_SEO_FIELDS = False

FILER_PAGINATE_BY = 50
FILER_IS_PUBLIC_DEFAULT = True
FILER_IMAGE_USE_ICON = True
FILER_ENABLE_PERMISSIONS = True
