from django.conf.urls import patterns, include, url
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from django.conf import settings

admin.autodiscover()

# urlpatterns = patterns('',
#     # Examples:
#     # url(r'^$', 'orange_website.views.home', name='home'),
#     # url(r'^orange_website/', include('orange_website.foo.urls')),
#
#     url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
#     url(r'^admin/', include(admin.site.urls)),
#     url(r'^docs/', include('sphinxdoc.urls')),
# )

urlpatterns = i18n_patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/cms/plugin/markup/', include('cmsplugin_markup.urls')),
    url(r'^docs/', include('sphinxdoc.urls')),
    url(r'^', include('cms.urls')),
)

if settings.DEBUG:
    urlpatterns = patterns('',
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
        {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
    url(r'', include('django.contrib.staticfiles.urls')),
) + urlpatterns
