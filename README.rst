*Orange Data Mining* main webpage
=============================

Development installation
------------------------

1. Clone from Bitbucket_ the webpage repository::

    hg clone https://bitbucket.org/biolab/orangepage

   You can also use SSH-based URLs or URLs of your forks.

2. Create and activate new `Python virtual environment`_::

    virtualenv --no-site-packages --distribute ~/.virtualenv/orange-website
    source ~/.virtualenv/orange-website/bin/activate

3. Move to location where you cloned webpage repository and run ``devsetup``
   script::

    python scripts/devsetup.py

   This script will install all requirements and import database dump to local database.

4. Fresh install::

    python manage.py syncdb --all
    python manage.py migrate --fake

4. Run::

    python manage.py runserver

   and start developing!

You can safely rerun the ``devsetup`` as needed, but be advised that local
database content **will NOT be preserved**.

.. _Bitbucket_: https://bitbucket.org/
.. _Python virtual environment: http://www.virtualenv.org
